import Vue from "vue";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyC-kvcJL-wQ6CN458FQG2eLCALXEp0UVb4",
    libraries: "places"
  }
});
